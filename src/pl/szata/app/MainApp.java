package pl.szata.app;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application {
 
	@Override
	public void start(Stage primaryStage) { 
		try {
			primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/pl/szata/view/MainPane.fxml"));
			Parent parent = (Parent) loader.load();
			
			Scene scene = new Scene(parent);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Address book");
			primaryStage.show();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
