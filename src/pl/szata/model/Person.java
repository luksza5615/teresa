package pl.szata.model;

import java.time.LocalDate;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Person {
	private StringProperty firstName;
	private StringProperty lastName;
	private ObjectProperty<LocalDate> birthday;
	private ObjectProperty<LocalDate> nameday;
	
	public Person() {
		this(null, null, null, null);
	}
	
	public Person(String firstName, String lastName) {
		this.firstName = new SimpleStringProperty(firstName);
		this.lastName = new SimpleStringProperty(lastName);
	}
	
	public Person(String firstName, String lastName, LocalDate birthday, LocalDate nameday) {
		this(firstName, lastName);
		this.birthday = new SimpleObjectProperty<LocalDate>(birthday);
		this.nameday = new SimpleObjectProperty<LocalDate>(nameday);
	}

	public String getFirstName() {
		return firstName.getValue();
	}
	
	public void setFirstName(String firstName) {
		this.firstName.setValue(firstName);
	}
	
	public StringProperty firstNameProperty() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName.getValue();
	}
	
	public void setLastName(String lastName) {
		this.lastName.setValue(lastName);
	}
	
	public StringProperty lastNameProperty() {
		return lastName;
	}
	
	public LocalDate getBirthday() {
		return birthday.getValue();
	}
	
	public void setBirthday(LocalDate birthday) {
		this.birthday.setValue(birthday);
	}
	
	public ObjectProperty<LocalDate> birthdayProperty() {
		return birthday;
	}
	
	public LocalDate getNameday() {
		return nameday.getValue();
	}
	
	public void setNameday(LocalDate nameday) {
		this.nameday.setValue(nameday);
	}
	
	public ObjectProperty<LocalDate> namedayProperty() {
		return nameday;
	}
}
