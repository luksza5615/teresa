package pl.szata.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateUtils {
    private static final String DATE_PATTERN = "dd LLL yyyy";
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_PATTERN);

    public static String dateToString(LocalDate date) {
        String temporar = null;

        if (date != null) {
            temporar = FORMATTER.format(date);
            return temporar.substring(0, temporar.length() - 4);
        } else {
            return null;
        }
    }

    private static LocalDate dateParser(String stringDate) {
        try {
            return LocalDate.parse(stringDate, FORMATTER);
        } catch (DateTimeParseException e) {
            return null;
        }
    }

    public static boolean validDate(String date) {
        return DateUtils.dateParser(date) != null;
    }
}
