package pl.szata.util;

import java.util.NoSuchElementException;

public enum Option {
    EXIT(0, "Wyjście z programu"),
    ADD_DATA(1, "Dodaj nową osobę"),
    PRINT_DATA(2, "Wyświetl dane o osobach"),
    REMOVE_PERSON(3, "Usuń osobę");

    private int option;
    private String description;

    Option(int option, String description) {
        this.option = option;
        this.description = description;
    }

    public static Option createFromInt(int option) {
        Option result = null;
        try {
            result = Option.values()[option];
        } catch (NoSuchElementException e) {
            System.err.println("Nie ma takiej opcji");
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public String toString() {
        return option + " - " + description;
    }
}
