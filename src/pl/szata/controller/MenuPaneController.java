package pl.szata.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;

public class MenuPaneController {

    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML
    private MenuItem menuDelete;
    @FXML
    private MenuItem menuEdit;
    @FXML
    private MenuItem menuAdd;
    @FXML
    private MenuItem menuClose;
    @FXML
    private MenuItem menuAbout;

    @FXML
    void menuAddAction(ActionEvent event) {
    }

    @FXML
    void menuDeleteAction(ActionEvent event) {
    }

    @FXML
    void menuEditAction(ActionEvent event) {
    }

    @FXML
    void menuCloseAction(ActionEvent event) {
    }

    @FXML
    void menuAboutAction(ActionEvent event) {
    }

    @FXML
    void initialize() {
    }
}
