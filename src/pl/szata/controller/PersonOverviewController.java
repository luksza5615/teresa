package pl.szata.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import pl.szata.app.MainApp;
import pl.szata.model.Person;

public class PersonOverviewController {

	@FXML
	private TableView<Person> personTable;
	@FXML
	private TableColumn<Person, String> firstNameColumn;
	@FXML
	private TableColumn<Person, String> lastNameColumn;
	@FXML
	private TextField firstNameField;
	@FXML
	private TextField lastNameField;
	@FXML
	private TextField birthdayField;
	@FXML
	private TextField namedayField;
    @FXML
    private Button deleteButton;
    @FXML
    private Button addButton;
    @FXML
    private Button editButton;
    @FXML
    public Button testButton;

    MainApp mainApp = new MainApp();
    Person person;	
     
	public Button getAddButton() {
	  	return addButton;
	}
	
	public TableView<Person> getPersonTable() {
		return personTable;
	}
	
	public TableColumn<Person, String> getFirstNameColumn() {
		return firstNameColumn;
	}

	public TableColumn<Person, String> getLastNameColumn() {
		return lastNameColumn;
	}

	public TextField getFirstNameField() {
		return firstNameField;
	}

	public TextField getLastNameField() {
		return lastNameField;
	}

	public TextField getBirthdayField() {
		return birthdayField;
	}

	public TextField getNamedayField() {
		return namedayField;
	}

	public Button getDeleteButton() {
		return deleteButton;
	}

	public Button getEditButton() {
		return editButton;
	}

	@FXML
	private void initialize() {
		configureTable();
	}

	public void configureTable() {
		firstNameColumn.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
		lastNameColumn.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
	}
}
