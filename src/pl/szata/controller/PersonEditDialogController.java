package pl.szata.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.szata.app.MainApp;
import pl.szata.model.Person;

import java.io.IOException;


public class PersonEditDialogController {

    @FXML
    private DatePicker namedayField;

    @FXML
    private Button cancelButton;

    @FXML
    private DatePicker birthdayField;

    @FXML
    private Button okButton;

    @FXML
    private TextField lastNameField;

    @FXML
    private TextField firstNameField;

    @FXML
    private Button clearButton;

    Person person;
    MainApp mainApp = new MainApp();
    MainController mainController;
    Stage dialogStage;

    public DatePicker getNamedayField() {
        return namedayField;
    }

    public void setNamedayField(DatePicker namedayField) {
        this.namedayField = namedayField;
    }

    public Button getCancelButton() {
        return cancelButton;
    }

    public void setCancelButton(Button cancelButton) {
        this.cancelButton = cancelButton;
    }

    public DatePicker getBirthdayField() {
        return birthdayField;
    }

    public void setBirthdayField(DatePicker birthdayField) {
        this.birthdayField = birthdayField;
    }

    public Button getOkButton() {
        return okButton;
    }

    public void setOkButton(Button okButton) {
        this.okButton = okButton;
    }

    public TextField getLastNameField() {
        return lastNameField;
    }

    public void setLastNameField(TextField lastNameField) {
        this.lastNameField = lastNameField;
    }

    public TextField getFirstNameField() {
        return firstNameField;
    }

    public void setFirstNameField(TextField firstNameField) {
        this.firstNameField = firstNameField;
    }

    public Button getClearButton() {
        return clearButton;
    }

    public void setClearButton(Button clearButton) {
        this.clearButton = clearButton;
    }

    public Stage getDialogStage() {
        return dialogStage;
    }

    public void initialize() {
        handleClearButton();
    }

    //set stage for dialog window
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void showPersonEditDialog(Person person) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/pl/szata/view/PersonEditDialog.fxml"));
            Parent personEditDialog = FXMLLoader.load(getClass().getResource("/pl/szata/view/PersonEditDialog.fxml"));

            Stage dialogStage = new Stage();
            Scene scene = new Scene(personEditDialog);
            dialogStage.setScene(scene);
            dialogStage.initModality(Modality.WINDOW_MODAL);
//            dialogStage.initOwner(mainApp.getPrimaryStage());
            setDialogStage(dialogStage);
            dialogStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //another way of handling events
    public void handleClearButton() {
        clearButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent action) {
                firstNameField.clear();
                lastNameField.clear();
                birthdayField.setValue(null);
                namedayField.setValue(null);
            }
        });
    }

    public void setPerson(Person person) {
        this.person = person;
        String imie = person.getFirstName();
        System.out.println(imie);

        if (person != null) {
            firstNameField.setText(person.getFirstName());
            lastNameField.setText(person.getLastName());
            birthdayField.setValue(person.getBirthday());
            namedayField.setValue(person.getNameday());
        } else {
            firstNameField.setText("LSLSLS");
            lastNameField.setText("");
            birthdayField.setValue(null);
            namedayField.setValue(null);
        }
    }

    @FXML
    public void handleCancel() {
        dialogStage.close();
    }

    @FXML
    public void handleOk() {
        if (inputValid()) {
            person.setFirstName(firstNameField.getText());
            person.setLastName(lastNameField.getText());
            person.setBirthday(birthdayField.getValue());
            person.setNameday(namedayField.getValue());
            dialogStage.close();
        }
    }

    public boolean inputValid() {
        String errorMessage = "";

        if (firstNameField.getLength() == 0) {
            errorMessage += "Wprowadź imię" + "\n";
        }

        if (lastNameField.getLength() == 0) {
            errorMessage += "Wprowadź nazwisko" + "\n";
        }
		
		/*
		if (birthdayField.getLength() == 0) {
			errorMessage += "Wprowadź datę urodzenia" + "\n";
		} else if (!DateUtils.validDate(birthdayField.getText())) {
			errorMessage += "Wprowadź datę urodzenia w formacie: dd.mm.rrrr" + "\n";
		}

		if (namedayField.getLength() == 0) {
			errorMessage += "Wprowadź datę imienin" + "\n";
		} else if(!DateUtils.validDate(namedayField.getText())) {
			errorMessage += "Wprowadź datę umienin w formacie: dd.mm.rrrr";
		}
		*/

        if (errorMessage.length() == 0) {
            return true;
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setHeaderText("");
            alert.setTitle("Niepoprawne dane");
            alert.setContentText(errorMessage);
            alert.showAndWait();
            return false;
        }
    }
}