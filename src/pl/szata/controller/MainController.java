package pl.szata.controller;

import java.time.LocalDate;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import pl.szata.model.Person;
import pl.szata.util.DateUtils;

public class MainController {

    @FXML
    private MenuPaneController menuPaneController;
    @FXML
    private PersonOverviewController personOverviewController;
    @FXML
    private PersonEditDialogController personEditDialogController;
    private ObservableList<Person> personData;

    private ObservableList<Person> getPersonData() {
        return personData;
    }

    public void initialize() {
        configureTable();
        handleDeleteButton();
        handleAddButton();
        handleEditButton();
    }

    private void configureTable() {
        TableView<Person> personTable = personOverviewController.getPersonTable();
        personData = FXCollections.observableArrayList();
        personTable.setItems(getPersonData());
        getPersonData().add(new Person("Jan", "Kowalski", LocalDate.of(1991, 11, 30), LocalDate.of(2018, 4, 9)));
        getPersonData().add(new Person("Maria", "Nowak", LocalDate.of(1991, 7, 30), LocalDate.of(2018, 9, 10)));

        showPersonDetails(null);

        personOverviewController.getPersonTable().getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showPersonDetails(newValue));
    }

    private void showPersonDetails(Person person) {
        if (person != null) {
            personOverviewController.getFirstNameField().setText(person.getFirstName());
            personOverviewController.getLastNameField().setText(person.getLastName());
            personOverviewController.getBirthdayField().setText(DateUtils.dateToString(person.getBirthday()));
            personOverviewController.getNamedayField().setText(DateUtils.dateToString(person.getNameday()));
        } else {
            personOverviewController.getFirstNameField().setText("");
            personOverviewController.getLastNameField().setText("");
            personOverviewController.getBirthdayField().setText("");
            personOverviewController.getNamedayField().setText("");
        }
    }

    private void handleDeleteButton() {
        Button deleteButton = personOverviewController.getDeleteButton();
        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                int selectedItem = personOverviewController.getPersonTable().getSelectionModel().getSelectedIndex();
                if (selectedItem >= 0) {
                    personOverviewController.getPersonTable().getItems().remove(selectedItem);
                } else {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Brak zaznaczenia");
                    alert.setHeaderText("Nie zaznaczono osoby");
                    alert.setContentText("Proszę zaznaczyć osobę w tabeli");
                    alert.showAndWait();
                }
            }
        });
    }

    private void handleEditButton() {
        Button editButton = personOverviewController.getEditButton();
        editButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Person selectedPerson = personOverviewController.getPersonTable().getSelectionModel().getSelectedItem();
                personEditDialogController = new PersonEditDialogController();
                personEditDialogController.showPersonEditDialog(selectedPerson);
            }
        });
    }

    private void handleAddButton() {
        Button addButton = personOverviewController.getAddButton();
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
            }
        });
    }
}